openload.co like to obfuscate the final download URL for their content.

This extension adds the final download URL to the webpage. It may be a bit
fragile and could break if they change their page layout.
