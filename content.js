if (document.querySelector("#main p:last-child")) {
  fetch(`https://openload.co/stream/${document.querySelector("#main p:last-child").textContent}?mime=true`, {
    method: 'HEAD'
  })
  .then(response => {
    let directLink = document.createElement('div')
    directLink.classList.add('content-text')
    directLink.style.fontSize = '13px'
    directLink.textContent = `axel -n 4 -a '${response.url}'`
    document.getElementsByClassName('file-details')[0].appendChild(directLink)
  })
  .catch(err => {})
}

